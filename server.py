import PIL
import PIL.Image
import cv2
import numpy as np
from flask import Flask, request
from flask_cors import CORS, cross_origin

import backend.SampleToPred

DEBUG = False

if DEBUG:
    from werkzeug.middleware.profiler import ProfilerMiddleware

app = Flask(__name__)

if DEBUG:
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

CLIENT_REQUIREMENT = "0.2.0"
SERVER_VERSION = "0.2.0"

ANALYZED_CHARS = 0


@app.route('/info')
@cross_origin()
def info():
    return {"version": {"server": SERVER_VERSION, "client": CLIENT_REQUIREMENT},
            "analyzed": ANALYZED_CHARS}


@app.route('/analyze', methods=['POST'])
@cross_origin()
def process_img():
    global ANALYZED_CHARS
    file = request.files['image']
    pil_image = PIL.Image.open(file.stream)
    # convert colorspace for cv2
    im = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGBA2BGR)

    # white on black
    im = cv2.bitwise_not(im)

    # Thicken Lines
    kernel = np.ones((2, 2), np.uint8)
    im = cv2.dilate(im, kernel, iterations=20)

    imgray = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
    contours, hierarchy = cv2.findContours(imgray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Area of interest
    x_min, x_max, y_min, y_max = find_box(contours)
    im = im[y_min:y_max, x_min:x_max]

    # resize to 28x28
    im = cv2.resize(im, [28, 28])

    im = PIL.Image.fromarray(im)
    # have ai analyze
    res = backend.SampleToPred.imageDataToPred(im)
    # return result
    letters = {}
    for letter, probability in res:
        letters[letter] = round(probability * 100, 2)

    ANALYZED_CHARS += 1

    return {"letters": letters}


def find_box(contours):
    x_min = 1000
    x_max = 0
    y_min = 1000
    y_max = 0
    for cl in list(contours):
        for e in list(cl):
            for c in e:
                if c[0] < x_min:
                    x_min = c[0]
                if c[0] > x_max:
                    x_max = c[0]
                if c[1] < y_min:
                    y_min = c[1]
                if c[1] > y_max:
                    y_max = c[1]

    return x_min, x_max, y_min, y_max


if __name__ == '__main__':
    app.run(debug=DEBUG, host="0.0.0.0", port=5000)
