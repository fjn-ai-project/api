Api glue for backend and frontend.

Installation:
- Install all packages in requirements.txt
- download backend into a folder named backend in the directory
- download frontend into a folder named frontend in the directory
- build flutter -> seee frontend readme

Usage:
- If flutter web: start flutter server with `python -m http.server 80`
- `python server.py`
- profit
